class AjaxController < ApplicationController
	def add_to_cart

		if session[:cart].blank?
			session[:cart] = Hash.new
		end

		item_id = params[:item_id]

		if !params[:adapter].blank?
			item_id += '_'+params[:adapter]
		end

		if !params[:height].blank?
			item_id += '_'+params[:height]
		end

		if !params[:rate].blank?
			item_id += '_'+params[:rate]
		end

		item_id += '_'+params[:type]

		if session[:cart][item_id].blank?
			start_count = 0
			session[:cart][item_id] = Hash.new
		else
			start_count = session[:cart][item_id]
		end

		
		session[:cart][item_id] = start_count + params[:count].to_i

		render :json => params[:item_id]
	end

	def del_from_cart
		if params[:item_id] == 'all'
			reset_session
			render :json => params[:items_id]
			return
		else
			session[:cart][params[:item_id]] = nil
			cart_empty = true
			session[:cart].each do |i|
				if !i[1].nil?
					cart_empty = false
				end
			end
			reset_session if cart_empty
			render :json => session[:cart]
		end
	end

	def ch_count
		if params[:count].to_i < 0
			new_count = 0
		else
			new_count = params[:count]
		end

		session[:cart][params[:item_id]] = new_count.to_i
		render :json => {item_id: params[:item_id], new_count: new_count, sess: session[:cart]}
		return
	end

	def get_price
		item_info = FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", params[:adapter], params[:height], params[:rate], params[:item_id])[0];
		render :json => { price: item_info.price.to_s, count: item_info.count_items }
		return
	end

  def get_delivery_price
		box_height = box_width = box_length = 0
		weight = 0
		session[:cart].each do |i|
			item_id = i[0].split('_')[0]
			adapter = height = rate = ''
			adapter = i[0].split('_')[1] if !i[0].split('_')[1].blank?
			height = i[0].split('_')[2] if !i[0].split('_')[2].blank?
			rate = i[0].split('_')[3] if !i[0].split('_')[3].blank?

			if adapter == 'Complect' || adapter == 'Hardware'
				if adapter == 'Complect'
					item = Complect.where(id: item_id).first
				else
					item = Hardware.where(id: item_id).first
				end
			else
				item = FilterItems.where(id: item_id).first
				item = FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0]
			end
			box_width = item.box_width if item.box_width > box_width
			box_length = item.box_length if item.box_length > box_length
			box_height += item.box_height.to_f
			weight += item.weight
		end
		volume = (box_width.to_f/1000)*(box_length.to_f/1000)*(box_height.to_f/1000)
		weight = weight.to_f/1000
		client = DL::API::Client.new(:appKey => "C06F6384-530D-11E5-97D2-00505683A6D3")
		result = client.request('public/calculator', {derivalPoint: '4000000200000000000000000', derivalDoor: false, arrivalPoint: params[:city], arrivalDoor: false, sizedVolume: volume, sizedWeight: weight})
		to_terminal = JSON.parse(result.body)["price"]
		result = client.request('public/calculator', {derivalPoint: '4000000200000000000000000', derivalDoor: false, arrivalPoint: params[:city], arrivalDoor: true, sizedVolume: volume, sizedWeight: weight})
		to_door = JSON.parse(result.body)["price"]

		render :json => {to_terminal: to_terminal, to_door: to_door}
	end
end