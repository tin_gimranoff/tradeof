# encoding: utf-8
require 'digest/md5'
require 'rest-client'
class SiteController < ApplicationController

	def index
		#client = DL::API::Client.new(:appKey => "C06F6384-530D-11E5-97D2-00505683A6D3")
		#puts client.request('public/cities', {})
		#return

		#client = DL::API::Client.new(:appKey => "C06F6384-530D-11E5-97D2-00505683A6D3")
		#result = client.request('countries', {:appKey => "C06F6384-530D-11E5-97D2-00505683A6D3"})

		#render :json => result
		# Если нужна автоизация:
		#session = client.auth({:username => "your_login", :password => "passw0rd"})
		#puts client.request('tracker', {:docId => "13-00083508789", :sessionID => session})

		#render :json => result

	end

	def textpage
		page = Textpage.where(slug: params[:url])
		@header = page[0].name
		@content = page[0].content
	end

	def faq
		@faq = Faq.order('id DESC')
	end


	def cart
		if @header.blank?
			@header = 'КОРЗИНА'
		end
	end

	def filters
		if !params[:per_page].blank?
			per_page = params[:per_page]
		else
			per_page = 10
		end

		if @header.blank?
			@header = 'ФИЛЬТРОЭЛЕМЕНТЫ'
		end
		if params[:sort].blank?
			sort = 'filter_configurations.price ASC'
		else
			sort = params[:sort]

			sort_type = sort.split(' ')[0].to_s
			if sort_type == 'price' || sort_type == 'rate'
				sort_type = 'filter_configurations.'+sort_type
			end
			sort = 'convert('+sort_type.to_s+', decimal) '+sort.split(' ')[1].to_s
		end


		query_str = Hash.new
		if !params[:filters].blank? || !params[:rate_to].blank? || !params[:rate_from].blank?
			if !params[:filters].blank?
				params[:filters].each do |index, i|
					query_str[index] = Array.new
					params[:filters][index].each do |value, j|
						 query_str[index] << '('+index+' LIKE "%'+value+'%")' 
					end 
				end
				query_str.each do |index, q|
					query_str[index.to_s] = "(" + query_str[index.to_s].join(" OR ") + ")"
				end 
			end

			query_str = query_str.map{|k, v| "#{v}"}.join(" AND ")

			if !query_str.blank?
				query_str = query_str + " AND filter_configurations.rate >="+params[:rate_from]+" AND filter_configurations.rate <="+params[:rate_to]
			else
				query_str = "filter_configurations.rate >="+params[:rate_from]+" AND filter_configurations.rate <="+params[:rate_to]
			end

			@items = FilterItems.joins(:filter_configuration).where(query_str).group('filter_items.id').paginate(:page => params[:page], :per_page => per_page).order(sort)
		else
			@items = FilterItems.joins(:filter_configuration).group('filter_items.id').paginate(:page => params[:page], :per_page => per_page).order(sort)
		end
	end 

	def filter
		@item_info = FilterItems.includes(:filter_configuration).find(params[:id])
	end

	def hardware
		if !params[:per_page].blank?
			per_page = params[:per_page]
		else
			per_page = 10
		end

		if @header.blank?
			@header = 'ОБОРУДОВАНИЕ'
		end
		if params[:sort].blank?
			sort = 'price ASC'
		else
			sort = params[:sort]
			sort = 'convert(`'+sort.split(' ')[0].to_s+'`, decimal) '+sort.split(' ')[1].to_s
		end


		query_str = Hash.new
		if !params[:filters].blank? || !params[:rate_to].blank? || !params[:rate_from].blank?
			if !params[:filters].blank?
				params[:filters].each do |index, i|
					query_str[index] = Array.new
					params[:filters][index].each do |value, j|
						 query_str[index] << '('+index+' LIKE "%'+value+'%")' 
					end 
				end
				query_str.each do |index, q|
					query_str[index.to_s] = "(" + query_str[index.to_s].join(" OR ") + ")"
				end 
			end

			query_str = query_str.map{|k, v| "#{v}"}.join(" AND ")

			@items = Hardware.where(query_str).paginate(:page => params[:page], :per_page => per_page).order(sort)
		else
			@items = Hardware.paginate(:page => params[:page], :per_page => per_page).order(sort)
		end
	end

	def hardware_details
		@item_info = Hardware.find(params[:id])
	end

	def complect
		if !params[:per_page].blank?
			per_page = params[:per_page]
		else
			per_page = 10
		end

		if @header.blank?
			@header = 'КОМПЛЕКТУЮЩИЕ'
		end
		if params[:sort].blank?
			sort = 'price ASC'
		else
			sort = params[:sort]
			sort = 'convert(`'+sort.split(' ')[0].to_s+'`, decimal) '+sort.split(' ')[1].to_s
		end


		query_str = Hash.new
		if !params[:filters].blank? || !params[:rate_to].blank? || !params[:rate_from].blank?
			if !params[:filters].blank?
				params[:filters].each do |index, i|
					query_str[index] = Array.new
					params[:filters][index].each do |value, j|
						 query_str[index] << '('+index+' LIKE "%'+value+'%")' 
					end 
				end
				query_str.each do |index, q|
					query_str[index.to_s] = "(" + query_str[index.to_s].join(" OR ") + ")"
				end 
			end

			query_str = query_str.map{|k, v| "#{v}"}.join(" AND ")

			@items = Complect.where(query_str).paginate(:page => params[:page], :per_page => per_page).order(sort)
		else
			@items = Complect.paginate(:page => params[:page], :per_page => per_page).order(sort)
		end
	end

	def complect_details
		@item_info = Complect.find(params[:id])
	end

	def payment
		if params[:order]
			params.permit!
			@order = Order.new(params[:order])
			if @order.valid?
				@order.cart = ''
				full_price = 0;
				box_height = box_width = box_length = 0
				box_weight = 0;
				if !session[:cart].blank?
					session[:cart].each do |i|
						item_id = i[0].split('_')[0]
						adapter = height = rate = ''
						adapter = i[0].split('_')[1] if !i[0].split('_')[1].blank?
						height = i[0].split('_')[2] if !i[0].split('_')[2].blank?
						rate = i[0].split('_')[3] if !i[0].split('_')[3].blank?

						if adapter == 'Complect' || adapter == 'Hardware'
						  if adapter == 'Complect'
								item_configuration = item = Complect.where(id: item_id).first
						  else
						    item_configuration = item = Hardware.where(id: item_id).first
						  end
						else
						    item = FilterItems.where(id: item_id).first
								item_configuration = FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0]
						end

						box_width = item_configuration.box_width if item_configuration.box_width > box_width
						box_length = item_configuration.box_length if item_configuration.box_length > box_length
						box_height += item_configuration.box_height.to_f
						box_weight += item_configuration.weight

						if adapter != 'Complect' && adapter != 'Hardware'
							@order.cart = @order.cart + "ID:"+item_id.to_s+"<br />Название: "+item.name+"<br />Адаптер: "+adapter+"<br />Высота: "+height+"<br />Рейтинг фильтрации: "+rate+"<br />Количество "+i[1].to_s+" шт<br /><br />"
						else
							@order.cart = @order.cart + "ID:"+item_id.to_s+"<br />Название: "+item.name+"<br />Количество "+i[1].to_s+" шт<br /><br />"  
						end
						
						if adapter == 'Complect' || adapter == 'Hardware'
							full_price = full_price + item.price.to_i*i[1].to_i
						else
							full_price = full_price + FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0].price.to_i*i[1]
						end
					end
				end

				city = Kladr.find(' '+params[:order][:city]).name

				@order.city = city

				volume = (box_width.to_f/1000)*(box_length.to_f/1000)*(box_height.to_f/1000)
				weight = box_weight.to_f/1000
				client = DL::API::Client.new(:appKey => "C06F6384-530D-11E5-97D2-00505683A6D3")

				if params[:order][:delivery] == 'До адреса'
					result = client.request('public/calculator', {derivalPoint: '4000000200000000000000000', derivalDoor: false, arrivalPoint: params[:order][:city], arrivalDoor: true, sizedVolume: volume, sizedWeight: weight})
					to_door = JSON.parse(result.body)["price"]
					full_price = full_price + to_door.to_f
				end

				if params[:order][:delivery] == 'До терминала'
					result = client.request('public/calculator', {derivalPoint: '4000000200000000000000000', derivalDoor: false, arrivalPoint: params[:order][:city], arrivalDoor: false, sizedVolume: volume, sizedWeight: weight})
					to_terminal = JSON.parse(result.body)["price"]
					full_price = full_price + to_terminal.to_f
				end

				@order.summa = sprintf("%.2f", full_price)
				@order.save
				if @order.payment == 'Robokassa'
					mrh_login = 'Tradeof.ru'
					mrh_pass1 = 'j3qq4h7h2v'
	                      		inv_id = @order.id
	                                inv_desc = 'tradeof.ru order'
	                                out_sum = @order.summa
	                                culture = 'ru'
					in_curr = ''
	                                encoding = 'utf-8'
					crc = Digest::MD5.hexdigest(mrh_login.to_s+':'+out_sum.to_s+':'+inv_id.to_s+':'+mrh_pass1.to_s)
					redirect_to 'http://test.robokassa.ru/Index.aspx?MrchLogin='+mrh_login.to_s+'&OutSum='+out_sum.to_s+'&InvId='+inv_id.to_s+'&IncCurrLabel='+in_curr.to_s+'&Desc='+inv_desc.to_s+'&SignatureValue='+crc.to_s+'&Culture='+culture.to_s+'&Encoding='+encoding.to_s
				end
				SiteMailer.send_order(@order).deliver
				RestClient.get 'http://smsc.ru/sys/send.php', {:params => {:charset => 'utf-8', :login => 'tradeof', :psw => '1531288', :phones => '79206176376', :mes => 'На сайте сделан новый заказ физическим лицом. Номер заказа: '+@order.id.to_s}}
			end
		else	
			full_price = 0;
				if !session[:cart].blank?
					session[:cart].each do |i|
						item_id = i[0].split('_')[0]
						adapter = height = rate = ''
						adapter = i[0].split('_')[1] if !i[0].split('_')[1].blank?
						height = i[0].split('_')[2] if !i[0].split('_')[2].blank?
						rate = i[0].split('_')[3] if !i[0].split('_')[3].blank?

						if adapter == 'Complect' || adapter == 'Hardware'
						  if adapter == 'Complect'
						    item = Complect.where(id: item_id).first
						  else
						    item = Hardware.where(id: item_id).first
						  end
						else
						    item = FilterItems.where(id: item_id).first 
						end

						if adapter == 'Complect' || adapter == 'Hardware'
							full_price = full_price + item.price.to_i*i[1].to_i
						else							
							full_price = full_price +  FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0].price.to_i*i[1]
						end
					end
				end
			@order = Order.new
			@order.summa = sprintf("%.2f", full_price)
		end
	end


	def payment_juri
		if params[:order]
			params.permit!
			if params[:fileio]
				@order = OrderPhisical.new(params[:order])
				@order.name = 'Заполнить из файла'
				@order.email = 'example@mail.ru'
				@order.address = 'Заполнить из файла'
				@order.inn = 'Заполнить из файла'
				@order.kpp = 'Заполнить из файла'
				save_path = Rails.root.join('public/users_docs', Date.today.to_time.to_i.to_s+'_'+params[:fileio].original_filename)
				content = File.read(params[:fileio].tempfile)
				File.open(save_path, 'wb') do |file|
					  file << content
				end
				@order.filename = save_path.to_s.gsub(Rails.root.join('public').to_s, "http://tradeof.ru")
				@order.status = 1
			else
				@order = OrderPhisical.new(params[:order]) 
			end
			if @order.valid? || params[:fileio]
				@order.cart = ''
				full_price = 0
				box_height = box_width = box_length = 0
				box_weight = 0
				if !session[:cart].blank?
					session[:cart].each do |i|
						item_id = i[0].split('_')[0]
						adapter = height = rate = ''
						adapter = i[0].split('_')[1] if !i[0].split('_')[1].blank?
						height = i[0].split('_')[2] if !i[0].split('_')[2].blank?
						rate = i[0].split('_')[3] if !i[0].split('_')[3].blank?

						if adapter == 'Complect' || adapter == 'Hardware'
						  if adapter == 'Complect'
								item_configuration = item = Complect.where(id: item_id).first
						  else
								item_configuration = item = Hardware.where(id: item_id).first
						  end
						else
						    item = FilterItems.where(id: item_id).first
								item_configuration = FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0]
						end

						box_width = item_configuration.box_width if item_configuration.box_width > box_width
						box_length = item_configuration.box_length if item_configuration.box_length > box_length
						box_height += item_configuration.box_height.to_f
						box_weight += item_configuration.weight

						if adapter != 'Complect' && adapter != 'Hardware'
							@order.cart = @order.cart + "ID:"+item_id.to_s+"<br />Название: "+item.name+"<br />Адаптер: "+adapter+"<br />Высота: "+height+"<br />Рейтинг фильтрации: "+rate+"<br />Количество "+i[1].to_s+" шт<br /><br />"
						else
							@order.cart = @order.cart + "ID:"+item_id.to_s+"<br />Название: "+item.name+"<br />Количество "+i[1].to_s+" шт<br /><br />"  
						end

						if adapter == 'Complect' || adapter == 'Hardware'
							full_price = full_price + item.price.to_i*i[1].to_i
						else
							full_price = full_price + FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0].price.to_i*i[1]
						end
					end
				end

				city = Kladr.find(' '+params[:order][:city]).name

				@order.city = city

				volume = (box_width.to_f/1000)*(box_length.to_f/1000)*(box_height.to_f/1000)
				weight = box_weight.to_f/1000
				client = DL::API::Client.new(:appKey => "C06F6384-530D-11E5-97D2-00505683A6D3")

				delivery = 0

				if params[:order][:delivery] == 'До адреса'
					result = client.request('public/calculator', {derivalPoint: '4000000200000000000000000', derivalDoor: false, arrivalPoint: params[:order][:city], arrivalDoor: true, sizedVolume: volume, sizedWeight: weight})
					to_door = JSON.parse(result.body)["price"]
					delivery = to_door.to_f
				end

				if params[:order][:delivery] == 'До терминала'
					result = client.request('public/calculator', {derivalPoint: '4000000200000000000000000', derivalDoor: false, arrivalPoint: params[:order][:city], arrivalDoor: false, sizedVolume: volume, sizedWeight: weight})
					to_terminal = JSON.parse(result.body)["price"]
					delivery = to_terminal.to_f
				end
				@order.summa = sprintf("%.2f", full_price.to_f+delivery.to_f)
				@order.summa_with_nds = sprintf("%.2f", full_price.to_f+(full_price.to_f/100*18)+delivery.to_f)
				@order.delivery_price = delivery
				if(@order.save && !params[:fileio])
					@pdf_html = render_to_string(:template => '/site/juri.html.erb', 
							:layout => false, 
							:encoding => "utf-8", 
							:locals => {
									:@order => @order,
									:@cart => session[:cart]
							})

					pdf = WickedPdf.new.pdf_from_string(@pdf_html)
					save_path = Rails.root.join('public/docs', @order.id.to_s+'_'+Date.today.to_time.to_i.to_s+'.pdf')
					File.open(save_path, 'wb') do |file|
					  file << pdf
					end

					@order.filename = save_path.to_s
					@order.save
					SiteMailer.send_doc(@order.email, save_path).deliver
					RestClient.get 'http://smsc.ru/sys/send.php', {:params => {:charset => 'utf-8', :login => 'tradeof', :psw => '1531288', :phones => '79206176376', :mes => 'На сайте сделан новый заказ юрилическим лицом. Номер заказа: '+@order.id.to_s}}
				end
			end
		else	
			full_price = 0;
				if !session[:cart].blank?
					session[:cart].each do |i|
						item_id = i[0].split('_')[0]

						adapter = height = rate = ''
						adapter = i[0].split('_')[1] if !i[0].split('_')[1].blank?
						height = i[0].split('_')[2] if !i[0].split('_')[2].blank?
						rate = i[0].split('_')[3] if !i[0].split('_')[3].blank?
						
						if adapter == 'Complect' || adapter == 'Hardware'
						  if adapter == 'Complect'
						    item = Complect.where(id: item_id).first
						  else
						    item = Hardware.where(id: item_id).first
						  end
						else
						    item = FilterItems.where(id: item_id).first 
						end


						if adapter == 'Complect' || adapter == 'Hardware'
							full_price = full_price + item.price.to_i*i[1].to_i
						else
							full_price = full_price + FilterConfiguration.where("adapter_type = ? AND height = ? AND rate = ? AND filter_items_id = ?", adapter, height, rate, item.id)[0].price.to_i*i[1]
						end
					end
				end
			@order = OrderPhisical.new
			@order.summa = sprintf("%.2f", full_price)
			@order.summa_with_nds = sprintf("%.2f", full_price.to_f + (full_price.to_f/100*18))
		end
	end

	def sitemap 
		@header = 'Карта сайта'
		map = Array.new
		map << { :url => '/', :name => 'Главная' }

		map << { :url => '/filters', :name => 'Фильтроэлементы', :children => Array.new }

		items = FilterItems.where(category: 1)

		items.each do |i|
			map[1][:children] << { :url => '/filters/'+i.id.to_s, :name => i.name }
		end

		map << { :url => '/hardware', :name => 'Оборудование', :children => Array.new }

		items = FilterItems.where(category: 2)

		items.each do |i|
			map[2][:children] << { :url => '/hardware/'+i.id.to_s, :name => i.name }
		end

		map << { :url => '/complect', :name => 'Комплектующие', :children => Array.new }

		items = FilterItems.where(category: 3)

		items.each do |i|
			map[3][:children] << { :url => '/complect/'+i.id.to_s, :name => i.name.to_s }
		end

		items = Textpage.all

		items.each do |i|
			map << { :url => '/'+i.slug, :name => i.name }
		end

		@map = map

	end
end
