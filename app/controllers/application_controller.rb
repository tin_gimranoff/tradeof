#coding: utf-8
require 'rest-client'
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :construct

  private 
  	def construct
  		@menu_items = Menu.where(:hide => 0, :parent => nil).order('ordernum ASC')
  		page_info = Seo.where(url: request.path)[0]
  		if !page_info.blank?
  				@title = page_info.title
  				@keywords = page_info.keywords
  				@description = page_info.description
  		end


      if params[:getcall]
        @getcall = Getcall.new(params[:getcall])
        if @getcall.valid?
          #тут будет смс оповещение
	        RestClient.get 'http://smsc.ru/sys/send.php', {:params => {:charset => 'utf-8', :login => 'tradeof', :psw => '1531288', :phones => '79206176376', :mes => 'Пришло сообщение - "Быстрый заказ". Имя отправителя: '+@getcall.name.to_s+' Телефон: '+@getcall.phone.to_s}}
        end
      else
        @getcall = Getcall.new
      end

      if params[:feedback]
        params.permit!
        @feedback = Feedback.new(params[:feedback])
        if @feedback.valid?
          @feedback.save
          #тут будет смс оповещение
	        RestClient.get 'http://smsc.ru/sys/send.php', {:params => {:charset => 'utf-8', :login => 'tradeof', :psw => '1531288', :phones => '79206176376', :mes => 'На сайт поступил новый отзыв. Вы можете просмотреть его в админ. панели под номером '+@feedback.id.to_s}}
        end
      else
        @feedback = Feedback.new
      end
  	end
end
