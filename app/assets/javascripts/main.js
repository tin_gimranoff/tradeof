$(document).ready(function(){

    var pay_robo_original = parseFloat($(".li_pay_robo").html());
    var pay_cash_original = parseFloat($(".li_pay_cash").html());

    var pay_with_nds = parseFloat($(".li_pay_with_nds").html());
    var pay_without_nds = parseFloat($(".li_pay_without_nds").html());

    $(".fancybox").fancybox();

    //Убираем робокассу если доставка компанией
    /*$(".delivery#company").bind('click', function(){
        $(".pay_robo").hide();
        $(".pay_cash input[type=radio]").click();
    });*/


    /*$(".delivery").bind('click', function(){
        if($(this).attr('id') != 'company')
        {
            $(".pay_robo").show();
        }
    });*/

    $("#check_to_terminal").bind('click', function(){
        var delivery_price = parseFloat($("#to_terminal").html());

        price_cash = pay_cash_original + delivery_price;
        price_robo = pay_robo_original + delivery_price;

        $(".li_pay_robo").empty().append(price_robo.toFixed(2)+" руб.");
        $(".li_pay_cash").empty().append(price_cash.toFixed(2)+" руб.");
    });

    $("#check_to_address").bind('click', function(){
        var delivery_price = parseFloat($("#to_address").html());

        price_cash = pay_cash_original + delivery_price;
        price_robo = pay_robo_original + delivery_price;

        $(".li_pay_robo").empty().append(price_robo.toFixed(2)+" руб.");
        $(".li_pay_cash").empty().append(price_cash.toFixed(2)+" руб.");

    });

    $("#check_original").bind('click', function(){
        $(".li_pay_robo").empty().append(pay_robo_original.toFixed(2)+" руб.");
        $(".li_pay_cash").empty().append(pay_cash_original.toFixed(2)+" руб.");
    });

    $("#check_original_juri").bind('click', function(){
        $(".li_pay_with_nds").empty().append(pay_with_nds.toFixed(2)+" руб.");
        $(".li_pay_without_nds").empty().append(pay_without_nds.toFixed(2)+" руб.");
    });

    $("#check_to_address_juri").bind('click', function(){
        var delivery_price = parseFloat($("#to_address").html());

        nds = pay_with_nds + delivery_price;
        without_nds = pay_without_nds + delivery_price;

        $(".li_pay_with_nds").empty().append(nds.toFixed(2)+" руб.");
        $(".li_pay_without_nds").empty().append(without_nds.toFixed(2)+" руб.");

    });

    $("#check_to_terminal_juri").bind('click', function(){
        var delivery_price = parseFloat($("#to_terminal").html());

        nds = pay_with_nds + delivery_price;
        without_nds = pay_without_nds + delivery_price;;

        $(".li_pay_with_nds").empty().append(nds.toFixed(2)+" руб.");
        $(".li_pay_without_nds").empty().append(without_nds.toFixed(2)+" руб.");
    });


    $("#count-minus").bind('click', function(){
        var val = $(".count-field").val();
        if(val-1 <= 0 )
            $(".count-field").attr('value', 1);
        else
            $(".count-field").attr('value', val-1);
    });

    $("#count-plus").bind('click', function(){
        var val = parseInt($(".count-field").val());
        $(".count-field").attr('value', val+1);
    });
 
    //Кнопка добавления в корзину
    $(".add-to-cart").bind('click', function(){
        var data = 'item_id='+$(this).attr('data-id');
        if($('select[name=adapter]').val())
            data += '&adapter='+$('select[name=adapter]').val();
        if($('select[name=height]').val())
            data += '&height='+$('select[name=height]').val();
        if($('select[name=rate]').val())
            data += '&rate='+$('select[name=rate]').val();
        data += '&type='+$(this).attr('data-type');
        data += '&count='+$(".count-field").val();
        $.ajax({
            url: '/ajax/add_to_cart',
            type: 'post',
            data: data,
            success: function(data) {
                if(data == -1)
                   //openform('success-form', 60, 285, 'ПОКУПКА ТОВАРА', 'В процессе произошла ошибка.'); 
                   alert('В процессе произошла ошибка.');
                else
                {
                    var current_items = $("#cart_items_count").text();
                    $("#cart_items_count").empty().append(parseInt(current_items)+parseInt($(".count-field").val()));
                    //openform('success-form', 60, 285, 'ПОКУПКА ТОВАРА', 'Товар успешно добавлен в корзину.<div style="height:10px"></div><a href="/cart"><img src="/images/goto_cart.png"></a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="close_form(this)"><img src="/images/goto_catalog.png"></a>');
                    alert('Товар успешно добавлен в корзину');
                    window.location.reload();
                } 
            },
            error: function(data) {
                 //openform('success-form', 60, 285, 'ПОКУПКА ТОВАРА', 'В процессе произошла ошибка.'); 
                 alert('В процессе произошла ошибка.');
            }
        });
        return false;
    });

    //Удаление елемента из корзины
    $(".rem-item").bind('click', function(){
        $.ajax({
            url: '/ajax/del_from_cart',
            type: 'post',
            data: 'item_id='+$(this).attr('data-id'),
            success: function(data) {
                window.location.reload();
            }
        });
        return false;
    });

    //Изменение количества товаров
    $(".cart-item-count").bind('input', function(){
        var val = $(this).val();
        var item_id = $(this).attr('item-id');
        $.ajax({
            url: '/ajax/ch_count',
            type: 'post',
            data: 'item_id='+item_id+'&count='+val,
            success: function(){
                window.location.reload();
            }
        });
    });

    $(".count-minus").bind('click', function(){
        var id = $(this).attr('item-id');
        $(".count-item[item-id='"+id+"']").attr('value', parseInt($(".count-item[item-id='"+id+"']").attr('value'))-1);
        var val = $(".count-item[item-id='"+id+"']").val();
        var item_id = $(".count-item[item-id='"+id+"']").attr('item-id');
        $.ajax({
            url: '/ajax/ch_count',
            type: 'post',
            data: 'item_id='+item_id+'&count='+val,
            success: function(){
                window.location.reload();
            }
        });
    });
    $(".count-plus").bind('click', function(){
        var id = $(this).attr('item-id');
        $(".count-item[item-id='"+id+"']").attr('value', parseInt($(".count-item[item-id='"+id+"']").attr('value'))+1);
        var val = $(".count-item[item-id='"+id+"']").val();
        var item_id = $(".count-item[item-id='"+id+"']").attr('item-id');
        $.ajax({
            url: '/ajax/ch_count',
            type: 'post',
            data: 'item_id='+item_id+'&count='+val,
            success: function(){
                window.location.reload();
            }
        });
    });

    //Минус параметр клик
    $(".param-minus").bind('click', function(){
        var El = $(this).parent().parent().find('input[type=textbox]')[0];
        if($(El).val() != 0)
        {
            var newVal = Number((parseFloat($(El).val())-parseFloat(0.10)).toFixed(3));
            $(El).attr('value', newVal);
            if($(this).attr('act-type') == 'from')
                $('.'+$(this).attr('attr')).val([newVal, null]);
            else
                $('.'+$(this).attr('attr')).val([null, newVal]);
        }
        return false;
    });


    //Плюс параметр клик
    $(".param-plus").bind('click', function(){
        var El = $(this).parent().parent().find('input[type=textbox]')[0];
        var newVal = Number((parseFloat($(El).val())+parseFloat(0.10)).toFixed(3));
        $(El).attr('value', newVal);
        if($(this).attr('act-type') == 'from')
            $('.'+$(this).attr('attr')).val([newVal, null]);
        else
            $('.'+$(this).attr('attr')).val([null, newVal]);
        return false;
    });

    //Закрытие формы
    $(".close-form").bind('click', function() {
        $(this).parent().hide();
        $(".overlay").hide();
    });

    //Ввод значения в поле
    $("input[type=textbox]").bind('input', function(){
        var newVal = $(this).val();
        if($(this).attr('name') == 'rate_from')
            $('.'+$(this).attr('attr')).val([newVal, null]);
        else
            $('.'+$(this).attr('attr')).val([null, newVal]);
        return false;
    });

    $("input[type=textbox]").bind('click', function(){
       $(this).select();
    });

    $('#slider-snap').bind('mouseenter', function(){
        $('input[type=textbox]').blur();
    });

    $("#city").bind('change', function(){
        var city = $(this).val();
        $.ajax({
            url: '/ajax/get_delivery_price',
            type: 'post',
            data: 'city='+city,
            success: function(data) {
                $("#to_address").empty().append(data.to_door+" руб.");
                $("#to_terminal").empty().append(data.to_terminal+" руб.");
                $("#check_original").click();
            }
        });
    });
});


function setValue(value) {
    if($('input[type=textbox]').is(':focus') == false)
    {
        var El = $(this).parent().find('input[type=textbox]');
        $(El).val(value);
        $(El).attr('value', value);
    }
}

function  getPageSize(){
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){ // Explorer 6 strict mode
        xScroll = document.documentElement.scrollWidth;
        yScroll = document.documentElement.scrollHeight;
    } else { // Explorer Mac...would also work in Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) { // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if(yScroll < windowHeight){
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if(xScroll < windowWidth){
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    return [pageWidth,pageHeight,windowWidth,windowHeight];
}

function openform(form_name, form_top, form_left, title, content)
{
    jQuery("."+form_name).css('top', getPageSize()[3]/2-form_top);
    jQuery("."+form_name).css('left', getPageSize()[2]/2-form_left);
    jQuery("#popup-title").empty().append(title);
    jQuery("#popup-content").empty().append(content);
    jQuery("."+form_name).css('display', 'block');
    jQuery(".overlay").show();
}

function close_form(El) {
    $(El).parent().parent().hide();
    $(".overlay").hide();
    return false;
}