#coding: utf-8
class SiteMailer < ActionMailer::Base
  default from: "robot@tradeof.ru"

  def send_doc(email, filename)
    attachments['doc.pdf'] = File.read(filename)
  	mail(to: email, subject: 'Счет') do |format|
      format.text { render "send_doc_email" }
    end
  end 


  def send_order(order)
  	email = order.email
  	mail(to: email, subject: 'Заказ в магазине tradeof.ru') do |format|
      format.text { render "send_email" }
    end
  end
end