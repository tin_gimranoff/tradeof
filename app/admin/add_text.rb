#coding: utf-8
ActiveAdmin.register AddText do


  menu parent: "Структура"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :label, :description, :content, :attached
  
  index do
    selectable_column
    id_column
    column :label
    column :description
    column :created_at
    column :updated_at
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая область" do
        f.input :label
        f.input :description
        f.input :attached, :required => false, :as => :file
        f.input :content,:as => :ckeditor
      end
      f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
