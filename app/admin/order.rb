#coding: utf-8
ActiveAdmin.register Order do

menu name: "Заказы"
permit_params :name, :phone, :email, :address, :comment, :status, :delivery, :payment, :cart, :summa, :city

  show do
    attributes_table do
      row :id
      row :name
      row :phone
      row :email
      row :city
      row :address
      row :comment
      row :status do |h|
        if h.status == 0
          'Не оплачен'
        else
          'Оплачен'
        end
      end
      row :delivery
      row :payment
      row :summa
      row :cart do |c|
        c.cart.html_safe
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  index do
    selectable_column
    id_column
    column :name
    column :phone
    column :email
    column :city
    column :address
    column :comment
    column :status do |h|
      if h.status == 0
        'Не оплачен'
      else
        'Оплачен'
      end
    end
    column :delivery
    column :payment
    column :summa
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
    f.inputs "Заказ" do
      f.input :name
      f.input :phone
      f.input :email
      f.input :city
      f.input :address, :as => :ckeditor
      f.input :comment, :as => :ckeditor
      f.input :status, as: :select, collection: [["Да", 1], ["Нет", 0]]
      f.input :delivery
      f.input :payment
      f.input :summa
    end
    f.actions
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
