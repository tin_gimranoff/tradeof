ActiveAdmin.register Complect do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model

menu parent: "Каталог"

permit_params :catalog_number, :name, :description, :description_full, :price, :box_length, :box_width, :box_height, :weight, :complect_category, :hidden, :surprice, :image, :image2, :image3, :image4, :count_items

  index do
    selectable_column
    id_column
    column :catalog_number
    column :name
    column :price
    column :box_length
    column :box_width
    column :box_height
    column :weight
    column :complect_category
    column :count_items
    column :hidden do |s|
      if s.hidden == 1
        'Да'
      else
        'Нет'
      end
    end
    column :surprice do |s|
      if s.surprice == 1
        'Да'
      else
        'Нет'
      end
    end
    actions
  end


 form(:html => {:multipart => true}) do |f|
      f.inputs "Новый товар" do
        f.input :catalog_number
        f.input :name
        f.input :complect_category
        f.input :price
        f.input :box_length
        f.input :box_width
        f.input :box_height
        f.input :weight
        f.input :description, :as => :ckeditor
        f.input :description_full, :as => :ckeditor
        f.input :count_items
        f.input :surprice, as: :select, collection: [["Нет", 0], ["Да", 1]]
        f.input :hidden, as: :select, collection: [["Нет", 0], ["Да", 1]]
        f.input :image, :required => false, :as => :file
        f.input :image2, :required => false, :as => :file
        f.input :image3, :required => false, :as => :file
        f.input :image4, :required => false, :as => :file
      end
      f.actions
  end
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

collection_action :import_items, :method => :post do 
    xls = Roo::Spreadsheet.open(params[:price].tempfile, :extension => 'xls')
    xls.default_sheet = xls.sheets[0]
    count_new = 0
    count_edit = 0
    error_count = 0
    for i in 2..xls.last_row
      item = Complect.where(catalog_number: xls.cell(i, 1))

      if item.empty?

        new_item = Complect.new

        new_item.catalog_number = xls.cell(i, 1).to_i
        new_item.name = xls.cell(i, 2).to_s
        new_item.description = xls.cell(i, 3).to_s
        new_item.description_full = xls.cell(i, 4).to_s
        new_item.price = xls.cell(i, 5).to_i
        new_item.complect_category = xls.cell(i, 6).mb_chars.downcase
        new_item.hidden = xls.cell(i, 7).to_i
        new_item.surprice = xls.cell(i, 8).to_i
        new_item.count_items = xls.cell(i, 9).to_i

        new_item.box_length = xls.cell(i, 10).to_i
        new_item.box_width = xls.cell(i, 11).to_i
        new_item.box_height = xls.cell(i, 12).to_i
        new_item.weight = xls.cell(i, 13).to_i

        if new_item.save
          count_new = count_new + 1
        else
          error_count = error_count + 1
        end
      else
        item = item[0]
        item.catalog_number = xls.cell(i, 1).to_i
        item.name = xls.cell(i, 2).to_s
        item.description = xls.cell(i, 3).to_s
        item.description_full = xls.cell(i, 4).to_s
        item.price = xls.cell(i, 5).to_i
        item.complect_category = xls.cell(i, 6).mb_chars.downcase
        item.hidden = xls.cell(i, 7).to_i
        item.surprice = xls.cell(i, 8).to_i
        #item.count_items = xls.cell(i, 9).to_i

        item.box_length = xls.cell(i, 10).to_i
        item.box_width = xls.cell(i, 11).to_i
        item.box_height = xls.cell(i, 12).to_i
        item.weight = xls.cell(i, 13).to_i

        if params[:residue].blank?
            item.count_items = item.count_items+xls.cell(i, 9).to_i
        else
            item.count_items = xls.cell(i, 9).to_i
        end

        if item.save
          count_edit = count_edit + 1
        else
          error_count = error_count + 1
        end
      end
    end 

    redirect_to '/admin', notice: "Завершено. Новых товаров: "+count_new.to_s+".Отредактировано: "+count_edit.to_s+". Ошибок: "+error_count.to_s+"."
end


end
