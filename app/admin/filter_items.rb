# ecoding: utf-8

require 'roo'

ActiveAdmin.register FilterItems do


menu parent: "Каталог"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :image, :filter_type, :matherial, :description, :department, :description_full, :surprice, :image2, :image3, :image4, :group_number


  index do
    selectable_column
    id_column
    column :group_number
    column :name
    column :filter_type
    column :surprice do |s|
      if s.surprice == 1
        'Да'
      else
        'Нет'
      end
    end
    actions
  end


    form(:html => {:multipart => true}) do |f|
      f.inputs "Новый товар" do
        f.input :group_number
        f.input :name
        f.input :filter_type
        f.input :matherial
        f.input :department
        f.input :description, :as => :ckeditor
        f.input :description_full, :as => :ckeditor
        f.input :surprice, as: :select, collection: [["Нет", 0], ["Да", 1]]
        f.input :image, :required => false, :as => :file
        f.input :image2, :required => false, :as => :file
        f.input :image3, :required => false, :as => :file
        f.input :image4, :required => false, :as => :file
      end
      f.actions
  end
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
collection_action :import_items, :method => :post do 
    xls = Roo::Spreadsheet.open(params[:price].tempfile, :extension => 'xls')
    xls.default_sheet = xls.sheets[0]
    count_new = 0
    count_edit = 0
    error_count = 0
    for i in 2..xls.last_row
      item = FilterItems.where(group_number: xls.cell(i, 1))
      if item.empty?
        new_item = FilterItems.new
        new_item.group_number = xls.cell(i, 1).to_i
        new_item.name = xls.cell(i, 2)
        new_item.filter_type = xls.cell(i, 3).mb_chars.downcase
        new_item.description = xls.cell(i, 4)
        new_item.description_full = xls.cell(i, 5)
        new_item.matherial = xls.cell(i, 6).mb_chars.downcase
        new_item.department = xls.cell(i, 7).mb_chars.downcase
        new_item.surprice = xls.cell(i, 8).to_i
        new_item.save
        if new_item.save
          count_new = count_new + 1
        else
          error_count = error_count + 1
        end
      else
        item[0].group_number = xls.cell(i, 1).to_i
        item[0].name = xls.cell(i, 2)
        item[0].filter_type = xls.cell(i, 3).mb_chars.downcase
        item[0].description = xls.cell(i, 4)
        item[0].description_full = xls.cell(i, 5)
        item[0].matherial = xls.cell(i, 6).mb_chars.downcase
        item[0].department = xls.cell(i, 7).mb_chars.downcase
        item[0].surprice = xls.cell(i, 8).to_i
        if item[0].save
          count_edit = count_edit + 1
        else
          error_count = error_count + 1
        end
      end
    end 
    
    redirect_to '/admin', notice: "Завершено. Новых товаров: "+count_new.to_s+". Отредактировано: "+count_edit.to_s+". Ошибок: "+error_count.to_s+"."
end 

end
