ActiveAdmin.register FilterConfiguration do


menu parent: "Каталог"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :catalog_number, :rate, :height, :adapter_type, :code, :price, :box_length, :box_width, :box_height, :weight, :hidden, :count_items, :filter_items_id

index do
    selectable_column
    id_column
    column :catalog_number
    column :rate
    column :height
    column :adapter_type
    column :code
    column :price
    column :box_length
    column :box_width
    column :box_height
    column :weight
    column :hidden
    column :count_items
    actions
end


    form(:html => {:multipart => true}) do |f|
      f.inputs "Новая конфигурация" do
        f.input :catalog_number
        f.input :rate
        f.input :height
        f.input :adapter_type
        f.input :code
        f.input :price
        f.input :box_length
        f.input :box_width
        f.input :box_height
        f.input :weight
        f.input :hidden, as: :select, collection: [["Нет", 0], ["Да", 1]]
        f.input :count_items
        f.input :filter_items_id, as: :select, collection: FilterItems.all.map{|i| ["#{i.name}", i.id]}
      end
      f.actions
  end
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

collection_action :import_items, :method => :post do 
    xls = Roo::Spreadsheet.open(params[:price].tempfile, :extension => 'xls')
    xls.default_sheet = xls.sheets[0]
    count_new = 0
    count_edit = 0
    error_count = 0
    for i in 2..xls.last_row
      item = FilterConfiguration.where(catalog_number: xls.cell(i, 2))

      filter_item = FilterItems.where(group_number: xls.cell(i, 1))

      if filter_item[0].blank?
            parent = nil
      else
            parent = filter_item[0].id
      end

      if item.empty?

        new_item = FilterConfiguration.new

        new_item.catalog_number = xls.cell(i, 2).to_i
        new_item.rate = xls.cell(i, 3).to_f
        new_item.height = xls.cell(i, 4).to_i
        new_item.adapter_type = xls.cell(i, 5)
        new_item.code = xls.cell(i, 6)
        new_item.price = xls.cell(i, 7).to_i
        new_item.box_length = xls.cell(i, 8).to_i
        new_item.box_width = xls.cell(i, 9).to_i
        new_item.box_height = xls.cell(i, 10).to_i
        new_item.weight = xls.cell(i, 11).to_i
        new_item.hidden = xls.cell(i, 12).to_i
        new_item.count_items = xls.cell(i, 13).to_i
        new_item.filter_items_id = parent

        if new_item.save
          count_new = count_new + 1
        else
          error_count = error_count + 1
        end
      else
        item = item[0]
        item.catalog_number = xls.cell(i, 2).to_i
        item.rate = xls.cell(i, 3).to_f
        item.height = xls.cell(i, 4).to_i
        item.adapter_type = xls.cell(i, 5).to_s
        item.code = xls.cell(i, 6).to_s
        item.price = xls.cell(i, 7).to_i
        item.box_length = xls.cell(i, 8).to_i
        item.box_width = xls.cell(i, 9).to_i
        item.box_height = xls.cell(i, 10).to_i
        item.weight = xls.cell(i, 11).to_i
        item.hidden = xls.cell(i, 12).to_i
        if params[:residue].blank?
            item.count_items = item.count_items+xls.cell(i, 13).to_i
        else
            item.count_items = xls.cell(i, 13).to_i
        end
        item.filter_items_id = parent

        if item.save
          count_edit = count_edit + 1
        else
          error_count = error_count + 1
        end
      end
    end 

    redirect_to '/admin', notice: "Завершено. Новых товаров: "+count_new.to_s+".Отредактировано: "+count_edit.to_s+". Ошибок: "+error_count.to_s+"."
end

end
