ActiveAdmin.register OrderPhisical do

menu name: "Заказы (Юр. лица)"
permit_params :name, :email, :address, :delivery, :cart, :summa, :summa_with_nds, :inn, :kpp, :filename, :with_nds, :status, :city

  show do
    attributes_table do
      row :id
      row :name
      row :inn
      row :kpp
      row :email
      row :city
      row :address
      row :delivery
      row :summa
      row :summa_with_nds
      row :cart do |c|
        c.cart.html_safe
      end
      row :filename
      row :with_nds do |c|
        if c.with_nds == 1
          "Да"
        else
          "Нет"
        end
      end
      row :status do |c|
        if c.status == true
          "Да"
        else
          "Нет"
        end
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  index do
    selectable_column
    id_column
    column :name
    column :email
    column :city
    column :address
    column :delivery
    column :summa
    column :summa_with_nds
    column :status do |c|
        if c.status == true
          "Да"
        else
          "Нет"
        end
      end
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
    f.inputs "Заказ" do
      f.input :name
      f.input :inn
      f.input :kpp
      f.input :email
      f.input :city
      f.input :address, :as => :ckeditor
      f.input :delivery
      f.input :summa
      f.input :summa_with_nds
      f.input :status
      f.input :with_nds
    end
    f.actions
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end



end
