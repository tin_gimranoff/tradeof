ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    

    # Here is an example of a simple dashboard with columns and panels.
    #
     columns do
       column do
         panel "Фильтроэлеметы (импорт товаров)" do
           form action: "/admin/filter_items/import_items", method: :post, enctype: "multipart/form-data" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :submit, value: "Импортировать товары"
           end
           form action: "/admin/filter_configurations/import_items", method: :post, enctype: "multipart/form-data", style: "margin-top: 20px;" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :submit, value: "Импортировать конфигурации"
           end
         end

         panel "Оборудование (импорт товаров)" do
           form action: "/admin/hardwares/import_items", method: :post, enctype: "multipart/form-data" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :submit, value: "Импортировать"
           end
         end

         panel "Комплектующие (импорт товаров)" do
           form action: "/admin/complects/import_items", method: :post, enctype: "multipart/form-data" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :submit, value: "Импортировать"
           end
         end

       end

       column do
         panel "Фильтроэлеметы (импорт остатков)" do
           form action: "/admin/filter_configurations/import_items", method: :post, enctype: "multipart/form-data" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :hidden, name: :residue, value: 1
                input type: :submit, value: "Импортировать конфигурации"
           end
         end

         panel "Оборудование (импорт остатков)" do
           form action: "/admin/hardwares/import_items", method: :post, enctype: "multipart/form-data" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :hidden, name: :residue, value: 1
                input type: :submit, value: "Импортировать"
           end
         end

         panel "Комплектующие (импорт остатков)" do
           form action: "/admin/complects/import_items", method: :post, enctype: "multipart/form-data" do
                input type: "hidden", name: request_forgery_protection_token.to_s, value: form_authenticity_token
                input type: :file, name: :price
                input type: :hidden, name: :residue, value: 1
                input type: :submit, value: "Импортировать"
           end
         end
         
       end

     end
  end # content
end
