class Order < ActiveRecord::Base
	validates :name, :address, :phone, :email, :city, presence: true

	validates :phone, :numericality => true
	validates :email, :format => {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
end
