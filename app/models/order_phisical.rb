class OrderPhisical < ActiveRecord::Base
	validates :name, :address, :email, :inn, :kpp, presence: true

	validates :email, :format => {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
end
