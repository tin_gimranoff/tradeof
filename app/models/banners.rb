class Banners < ActiveRecord::Base
  has_attached_file :image, :path => ":rails_root/public/:class/:attachment/:id/:basename.:extension", :url => "/:class/:attachment/:id/:basename.:extension"

  validates :label, :url, :description, presence: true

  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def self.get_banner(label, class_name)
    banner = self.where('label = ?', label)[0]
    if banner.url == '#'
      return '<img src="'+banner.image.url+'" />'
    else
      return '<a href="'+banner.url+'"><img src="'+banner.image.url+'" /></a>'
    end
  end

  def self.get_banners(label, class_name = '', type = '')
    banner = self.where('label = ?', label)
    html = ''
    banner.each.with_index do |b, index|
      if type == 'images'
          if b.url == '#'
             html += ActionController::Base.helpers.image_tag b.image.url, :class => class_name, :rel => label, :style => ("display: inline" if index == 0)
          else
            html += ActionController::Base.helpers.link_to (ActionController::Base.helpers.image_tag b.image.url, :class => class_name, :rel => label, :style => ("display: inline" if index == 0)), b.url
          end
      end
      if type == 'markers'
        html += '<li class="'+class_name+'">'+(ActionController::Base.helpers.image_tag b.image.url)+'</li>'
      end 
    end
    return html.html_safe
  end

end
