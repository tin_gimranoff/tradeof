class Complect < ActiveRecord::Base
	has_attached_file :image, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image2, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image3, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image4, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image2, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image3, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image4, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	validates :catalog_number, numericality: { only_integer: true }

	validates :box_length, numericality: { only_integer: true }
	validates :box_width, numericality: { only_integer: true }
	validates :box_height, numericality: { only_integer: true }
	validates :weight, numericality: { only_integer: true }

	def self.get_category
		categories = self.select(:complect_category).order(:complect_category).uniq

		exploded_categories = Array.new

		categories.each do |t|
			items = t.complect_category.split(/,/)
			items.each do |i|
				exploded_categories << i.strip.mb_chars.downcase
			end
		end

		index = 0
		while index < exploded_categories.count do
				if exploded_categories.count(exploded_categories[index]) > 1
					exploded_categories.delete_at(index)
				else
					index = index + 1
				end
		end

		return exploded_categories
	end
end
