# encoding: utf-8
class FilterItems < ActiveRecord::Base
	has_attached_file :image, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image2, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image3, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image4, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image2, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image3, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image4, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	validates :group_number, numericality: { only_integer: true }

	has_many :filter_configuration
#адаптер высота ретинг фильтрации

	def self.get_price_hash(price)
				 prices = price.split(';') 
				 price_config = Hash.new
                 prices.each do |p|
                    p_cfg = p.split('-')
                    if price_config[p_cfg[0]].blank?
                    	price_config[p_cfg[0]] = Hash.new
                    end
                    price_config[p_cfg[0]][p_cfg[1]] = p_cfg[2]
              	 end
              	 return price_config 
	end

	def self.get_price_from_params(prices, height, rate)
		price_config = get_price_hash(prices)
		return price_config[height][rate]
	end

	def self.get_matherial
		matherial = self.select(:matherial).order(:matherial).uniq

		exploded_matherials = Array.new

		matherial.each do |m|
			items = m.matherial.split(/,/)
			items.each do |i|
				exploded_matherials << i.strip.mb_chars.downcase
			end
		end

		index = 0
		while index < exploded_matherials.count do
				if exploded_matherials.count(exploded_matherials[index]) > 1
					exploded_matherials.delete_at(index)
				else
					index = index + 1
				end
		end

		return exploded_matherials
	end


	def self.get_type
		types = self.select(:filter_type).order(:filter_type).uniq

		exploded_types = Array.new

		types.each do |t|
			items = t.filter_type.split(/,/)
			items.each do |i|
				exploded_types << i.strip.mb_chars.downcase
			end
		end

		index = 0
		while index < exploded_types.count do
				if exploded_types.count(exploded_types[index]) > 1
					exploded_types.delete_at(index)
				else
					index = index + 1
				end
		end

		return exploded_types
	end

	def self.get_department
		types = self.select(:department).order(:department).uniq

		exploded_dep = Array.new

		types.each do |t|
			items = t.department.split(/,/)
			items.each do |i|
				exploded_dep << i.strip.mb_chars.downcase
			end
		end

		index = 0
		while index < exploded_dep.count do
				if exploded_dep.count(exploded_dep[index]) > 1
					exploded_dep.delete_at(index)
				else
					index = index + 1
				end
		end

		return exploded_dep
	end

	def get_adapters
		adapters = Array.new
		self.filter_configuration.each do |i|
			adapters << i.adapter_type
		end

		return adapters.uniq
	end

	def get_heights
		heights = Array.new
		self.filter_configuration.each do |i|
			heights << i.height
		end

		return heights.uniq
	end

	def get_rates
		rates = Array.new
		self.filter_configuration.each do |i|
			rates << i.rate
		end

		return rates.uniq
	end
end
