class FilterConfiguration < ActiveRecord::Base

	validates :catalog_number, numericality: { only_integer: true }
	validates :rate, numericality: true
	validates :height, numericality: { only_integer: true }
	validates :price, numericality: { only_integer: true }
	validates :box_length, numericality: { only_integer: true }
	validates :box_width, numericality: { only_integer: true }
	validates :box_height, numericality: { only_integer: true }
	validates :weight, numericality: { only_integer: true }
	validates :count_items, numericality: { only_integer: true }


	belongs_to :filter_items, dependent: :destroy
end
