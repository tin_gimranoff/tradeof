class Textpage < ActiveRecord::Base
	validates :name, :slug, :content, presence: true
end
