class Hardware < ActiveRecord::Base
	has_attached_file :image, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image2, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image3, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"
	has_attached_file :image4, :styles => { :thumb => "200x176>", :inner => "360x316>" }, :path => ":rails_root/public/:class/:attachment/:id/:style/:basename.:extension", :url => "/:class/:attachment/:id/:style/:basename.:extension"

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image2, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image3, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :image4, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	validates :catalog_number, numericality: { only_integer: true }

	validates :box_length, numericality: { only_integer: true }
	validates :box_width, numericality: { only_integer: true }
	validates :box_height, numericality: { only_integer: true }
	validates :weight, numericality: { only_integer: true }

	def self.get_holder
		holders = self.select(:holder).order(:holder).uniq

		exploded_holders = Array.new

		holders.each do |t|
			items = t.holder.split(/,/)
			items.each do |i|
				exploded_holders << i.strip.mb_chars.downcase
			end
		end

		index = 0
		while index < exploded_holders.count do
				if exploded_holders.count(exploded_holders[index]) > 1
					exploded_holders.delete_at(index)
				else
					index = index + 1
				end
		end

		return exploded_holders
	end

	def self.get_matherial
		matherial = self.select(:matherial).order(:matherial).uniq

		exploded_matherials = Array.new

		matherial.each do |m|
			items = m.matherial.split(/,/)
			items.each do |i|
				exploded_matherials << i.strip.mb_chars.downcase
			end
		end

		index = 0
		while index < exploded_matherials.count do
				if exploded_matherials.count(exploded_matherials[index]) > 1
					exploded_matherials.delete_at(index)
				else
					index = index + 1
				end
		end

		return exploded_matherials
	end
end
