class Seo < ActiveRecord::Base
	validates :url, :title, :keywords, :description,  presence: true
end
