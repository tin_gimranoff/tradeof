require 'httparty'
require 'json'

module DL
  module API
    class Client

      def initialize(options = {})
        raise "No app key given" unless options[:appKey]
        @app_key = options[:appKey]
      end

      def auth(options = {})        
          response = HTTParty.post('https://api.dellin.ru/v1/login.json', 
            :body => {:login => options[:username], :password => options[:password], :appKey => @app_key}.to_json,
            :headers => {'Content-Type' => 'application/json'})
          print response
          return response['sessionID']
      end

      def request(op, params)
        params[:appKey] = @app_key
        response = HTTParty.post("https://api.dellin.ru/v1/#{op}.json",
          :body => params.to_json,
          :headers => {'Content-Type' => 'application/json'})
        return response
      end
    end
  end
end

#Пример использования

client = DL::API::Client.new(:appKey => "111")
puts client.request('tracker', {:docId => "13-00083508789"})
# Если нужна автоизация:
session = client.auth({:username => "your_login", :password => "passw0rd"})
puts client.request('tracker', {:docId => "13-00083508789", :sessionID => session})

