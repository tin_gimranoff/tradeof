# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151010155057) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "add_texts", force: true do |t|
    t.string   "label"
    t.string   "description"
    t.text     "content"
    t.string   "attached_file_name"
    t.string   "attached_content_type"
    t.integer  "attached_file_size"
    t.datetime "attached_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "banners", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "label"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "complects", force: true do |t|
    t.integer  "catalog_number",      limit: 8
    t.string   "name"
    t.string   "description"
    t.text     "description_full"
    t.integer  "price"
    t.text     "complect_category"
    t.integer  "hidden",                        default: 0
    t.integer  "surprice",                      default: 0
    t.integer  "count_items",                   default: 0
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "image2_file_name"
    t.string   "image2_content_type"
    t.integer  "image2_file_size"
    t.datetime "image2_updated_at"
    t.string   "image3_file_name"
    t.string   "image3_content_type"
    t.integer  "image3_file_size"
    t.datetime "image3_updated_at"
    t.string   "image4_file_name"
    t.string   "image4_content_type"
    t.integer  "image4_file_size"
    t.datetime "image4_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "box_length",                    default: 0
    t.integer  "box_width",                     default: 0
    t.integer  "box_height",                    default: 0
    t.integer  "weight",                        default: 0
  end

  create_table "faqs", force: true do |t|
    t.string   "question"
    t.text     "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "filter_configurations", force: true do |t|
    t.integer  "catalog_number",  limit: 8
    t.decimal  "rate",                      precision: 11, scale: 2
    t.integer  "height"
    t.string   "adapter_type"
    t.string   "code"
    t.integer  "price"
    t.integer  "box_length"
    t.integer  "box_width"
    t.integer  "box_height"
    t.integer  "weight"
    t.boolean  "hidden"
    t.integer  "count_items"
    t.integer  "filter_items_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "filter_items", force: true do |t|
    t.string   "name"
    t.string   "filter_type"
    t.string   "matherial"
    t.text     "department"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description_full"
    t.integer  "surprice",            default: 0
    t.string   "image2_file_name"
    t.string   "image2_content_type"
    t.integer  "image2_file_size"
    t.datetime "image2_updated_at"
    t.string   "image3_file_name"
    t.string   "image3_content_type"
    t.integer  "image3_file_size"
    t.datetime "image3_updated_at"
    t.string   "image4_file_name"
    t.string   "image4_content_type"
    t.integer  "image4_file_size"
    t.datetime "image4_updated_at"
    t.integer  "group_number"
  end

  create_table "hardwares", force: true do |t|
    t.integer  "catalog_number",      limit: 8
    t.string   "name"
    t.string   "description"
    t.text     "description_full"
    t.integer  "price"
    t.string   "holder"
    t.string   "matherial"
    t.integer  "hidden",                        default: 0
    t.integer  "surprice",                      default: 0
    t.integer  "count_items",                   default: 0, null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "image2_file_name"
    t.string   "image2_content_type"
    t.integer  "image2_file_size"
    t.datetime "image2_updated_at"
    t.string   "image3_file_name"
    t.string   "image3_content_type"
    t.integer  "image3_file_size"
    t.datetime "image3_updated_at"
    t.string   "image4_file_name"
    t.string   "image4_content_type"
    t.integer  "image4_file_size"
    t.datetime "image4_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "box_length",                    default: 0
    t.integer  "box_width",                     default: 0
    t.integer  "box_height",                    default: 0
    t.integer  "weight",                        default: 0
  end

  create_table "kladrs", primary_key: "codeKLADR", force: true do |t|
    t.string  "id",         null: false
    t.string  "name",       null: false
    t.boolean "isTerminal", null: false
  end

  add_index "kladrs", ["id"], name: "id", unique: true, using: :btree

  create_table "menus", force: true do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "ordernum"
    t.integer  "hide"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent"
  end

  create_table "order_phisicals", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "address"
    t.string   "inn"
    t.string   "kpp"
    t.string   "delivery"
    t.text     "cart"
    t.string   "summa"
    t.string   "summa_with_nds"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "filename"
    t.integer  "with_nds"
    t.boolean  "status",         default: false
    t.string   "city"
    t.integer  "delivery_price"
  end

  create_table "orders", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "address"
    t.string   "comment"
    t.integer  "status",     default: 0
    t.string   "delivery"
    t.string   "payment"
    t.text     "cart"
    t.string   "summa"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
  end

  create_table "seos", force: true do |t|
    t.string   "url"
    t.string   "title"
    t.text     "keywords"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "settings", force: true do |t|
    t.string   "param_name"
    t.string   "param_key"
    t.string   "param_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "textpages", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
