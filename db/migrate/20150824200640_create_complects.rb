class CreateComplects < ActiveRecord::Migration
  def change
    create_table :complects do |t|
      t.integer :catalog_number
      t.string :name
      t.string :description
      t.text :description_full
      t.integer :price
      t.text :complect_category
      t.integer :hidden, :default => 0
      t.integer :surprice, :default => 0
      t.integer :count_items, :default => 0
      t.has_attached_file :image
      t.has_attached_file :image2
      t.has_attached_file :image3
      t.has_attached_file :image4

      t.timestamps
    end
  end
end
