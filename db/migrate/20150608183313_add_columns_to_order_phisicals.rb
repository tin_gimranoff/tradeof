class AddColumnsToOrderPhisicals < ActiveRecord::Migration
  def change
    add_column :order_phisicals, :filename, :string
    add_column :order_phisicals, :with_nds, :integer
    add_column :order_phisicals, :status, :boolean, :default => false
  end
end
