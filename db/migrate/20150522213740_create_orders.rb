class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :address
      t.string :comment
      t.integer :status, :default => 0
      t.string :delivery
      t.string :payment
      t.text :cart
      t.string :summa

      t.timestamps
    end
  end
end
