class CreateFilters < ActiveRecord::Migration
  def change
    create_table :filter_items do |t|
      t.string :name
      t.string :filter_type
      t.string :rate
      t.string :matherial
      t.string :department
      t.string :price
      t.text :description
      t.has_attached_file :image

      t.timestamps
    end
  end
end
