class AddColumnsToComplects < ActiveRecord::Migration
  def change
    add_column :complects, :box_length, :integer, :default => 0
    add_column :complects, :box_width, :integer, :default => 0
    add_column :complects, :box_height, :integer, :default => 0
    add_column :complects, :weight, :integer, :default => 0
  end
end
