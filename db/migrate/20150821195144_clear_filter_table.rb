class ClearFilterTable < ActiveRecord::Migration
  def change
  	remove_column :filter_items, :rate
  	remove_column :filter_items, :adapter
  	remove_column :filter_items, :height
  	remove_column :filter_items, :count_items
  	remove_column :filter_items, :holder
  	remove_column :filter_items, :setting
  	remove_column :filter_items, :category
  	remove_column :filter_items, :complect_categroy
  	remove_column :filter_items, :price



  	add_column :filter_items, :group_number, :integer
  end
end
