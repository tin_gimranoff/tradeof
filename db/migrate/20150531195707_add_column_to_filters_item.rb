class AddColumnToFiltersItem < ActiveRecord::Migration
  def change
  	add_column :filter_items, :holder, :string
  	add_column :filter_items, :setting, :string
  	add_column :filter_items, :category, :integer, :default => 1
  	add_column :filter_items, :complect_categroy, :string
  end
end
