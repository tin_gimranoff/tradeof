class AddColumnsToFilterItem < ActiveRecord::Migration
  def change
    add_column :filter_items, :surprice, :integer, :default => 0
    add_attachment :filter_items, :image2
    add_attachment :filter_items, :image3
    add_attachment :filter_items, :image4
  end
end
