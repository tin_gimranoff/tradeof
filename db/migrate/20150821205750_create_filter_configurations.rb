class CreateFilterConfigurations < ActiveRecord::Migration
  def change
    create_table :filter_configurations do |t|
      t.integer :catalog_number
      t.integer :rate
      t.integer :height
      t.string :adapter_type
      t.string :code
      t.integer :price
      t.integer :box_length
      t.integer :box_width
      t.integer :box_height
      t.integer :weight
      t.boolean :hidden
      t.integer :count_items
      t.references :filter_items

      t.timestamps
    end
  end
end
