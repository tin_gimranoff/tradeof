class CreateOrderPhisicals < ActiveRecord::Migration
  def change
    create_table :order_phisicals do |t|
      t.string :name
      t.string :email
      t.string :address
      t.string :inn
      t.string :kpp
      t.string :delivery
      t.text :cart
      t.string :summa
      t.string :summa_with_nds

      t.timestamps
    end
  end
end
