class AddParamsToFilterItems < ActiveRecord::Migration
  def change
  	add_column :filter_items, :description_full, :text
  	add_column :filter_items, :adapter, :string	
  	add_column :filter_items, :height, :string
  	add_column :filter_items, :count_items, :integer, :default => 0
  end
end
