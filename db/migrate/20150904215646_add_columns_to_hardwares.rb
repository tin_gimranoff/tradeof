class AddColumnsToHardwares < ActiveRecord::Migration
  def change
    add_column :hardwares, :box_length, :integer, :default => 0
    add_column :hardwares, :box_width, :integer, :default => 0
    add_column :hardwares, :box_height, :integer, :default => 0
    add_column :hardwares, :weight, :integer, :default => 0
  end
end
