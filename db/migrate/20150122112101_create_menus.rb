class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :name
      t.string :url
      t.integer :ordernum
      t.integer :hide

      t.timestamps
    end
  end
end
