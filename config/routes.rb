Rails.application.routes.draw do
  constraints subdomain: 'www' do
    get ':any', to: redirect(subdomain: nil, path: '/%{any}'), any: /.*/
  end
  
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'site#index', via: ['get', 'post']
  match 'faq' => 'site#faq', via: ['get', 'post']
  match 'filters' => 'site#filters', via: ['get', 'post']
  match 'cart' => 'site#cart', via: ['get', 'post']
  match '/filter/:id' => 'site#filter', via: ['get', 'post']
  match 'payment' => 'site#payment', via: ['get', 'post']
  match 'payment/juridicial' => 'site#payment_juri', via: ['get', 'post']

  match 'hardware' => 'site#hardware', via: ['get', 'post']
  match '/hardware/:id' => 'site#hardware_details', via: ['get', 'post']

  match 'complect' => 'site#complect', via: ['get', 'post']
  match '/complect/:id' => 'site#complect_details', via: ['get', 'post']

  match 'payment/rest' => 'payment#rest', via: ['get', 'post']
  match 'payment/success' => 'payment#success', via: ['get', 'post']
  match 'payment/fail' => 'payment#fail', via: ['get', 'post']

  match 'sitemap' => 'site#sitemap', via: ['get', 'post']

  post 'ajax/add_to_cart' => 'ajax#add_to_cart'
  post 'ajax/del_from_cart' => 'ajax#del_from_cart'
  post 'ajax/ch_count' => 'ajax#ch_count'
  post 'ajax/get_price' => 'ajax#get_price'
  match 'ajax/get_delivery_price' => 'ajax#get_delivery_price', via: ['get', 'post']


  match ':url' => 'site#textpage', via: ['get', 'post']

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
